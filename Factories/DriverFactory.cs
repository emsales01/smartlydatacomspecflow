﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;

namespace SmartlyExercise.Factories
{
    public class DriverFactory
    {
        public IWebDriver CreateDriver()
        {
            string browser = Environment.GetEnvironmentVariable("BROWSER") ?? "CHROME";

            switch (browser.ToUpperInvariant())
            {
                case "CHROME":
                    return new ChromeDriver();

                case "EDGE":
                    return new EdgeDriver();

                case "FIREFOX":
                    return new FirefoxDriver();

                default:
                    throw new ArgumentException($"Browser not yet implemented: {browser}");
            }
        }
    }
}