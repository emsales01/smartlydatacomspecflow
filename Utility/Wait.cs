﻿using System.Diagnostics;
using OpenQA.Selenium;

namespace SmartlyExercise.Utility
{
    public static class Wait
    {
        public static IWebElement ForEnabled(this IWebElement element, int timeSpan = 10000)
        {
            Stopwatch watch = new Stopwatch();

            watch.Start();
            while (watch.Elapsed.Milliseconds < timeSpan)
            {
                if (element.Enabled)
                    return element;
            }

            throw new ElementNotInteractableException();
        }

        public static IWebElement ForVisible(this IWebElement element, int timeSpan = 2000)
        {
            Stopwatch watch = new Stopwatch();

            watch.Start();
            while (watch.Elapsed.Milliseconds < timeSpan)
            {
                if (element.Displayed)
                    return element;
            }

            throw new ElementNotVisibleException();
        }

        public static IWebElement ForText(this IWebElement element, int timeSpan = 10000)
        {
            Stopwatch watch = new Stopwatch();

            watch.Start();
            while (watch.Elapsed.Milliseconds < timeSpan)
            {
                if (element.Text.Length > 0)
                    return element;
            }

            throw new ElementNotVisibleException();
        }
    }
}