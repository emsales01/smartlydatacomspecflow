﻿Feature: Payees Page Validations

As a user, I can verify the functionalities of Payees Page
Background: 
	Given Enter the Demo BNZ URL
	When Navigate to Payees Page


@TestCase:1
Scenario:  Verify you can navigate to Payees page using the navigation menu
	Then Verify Payees page is loaded

@TestCase:2
Scenario Outline:  Verify you can add new payee in the Payees page
	When Click Add Button
	And  Enter the payee details <Payee> and <Payee Name>
	And  Click Add Payee Button
	Then <Message> is displayed, and payee is added in the list of payees
	Examples: 
	| Payee | Payee Name            | Message		|
	| tyco  | TYCO CYLINDER TESTING | Payee added   |

@TestCase:3
Scenario:  Verify payee name is a required field
	When Click Add Button
	And  Click Add Disabled Payee Button
	And  Validate Required Field Error
	And  Enter the payee details <Payee> and <Payee Name>
	Then Validate errors are gone
	Examples: 
	| Payee   | Payee Name            |
	| tyco    | TYCO CYLINDER TESTING |

@TestCase:4
Scenario:  Verify that payees can be sorted by name
	When Verify list is sorted in ascending order by default
	And  Click Name header
	Then Verify list is sorted in descending order
