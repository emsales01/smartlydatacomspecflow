﻿Feature: Payments Page Validations

As a user, I can verify the functionalities of Payments Page

Background: 
	Given Enter the Demo BNZ URL
	When Navigate to Payments Page


@TestCase:5
Scenario Outline:  Navigate to Payments page
	When Transfer <amount> from Everyday account to Bills account & Check the current balance of are correct
	Then Transfer successful message is displayed
	Examples: 
	| amount |
	| 500    |


