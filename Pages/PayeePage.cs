﻿using System.Collections;
using NUnit.Framework;
using OpenQA.Selenium;
using Wait = SmartlyExercise.Utility.Wait;

namespace SmartlyExercise.Pages
{
    public class PayeePage
    {
        public IWebDriver driver;

        public PayeePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement addButton => driver.FindElement(By.XPath("(//button[@type='button']//span)[2]"));
        private IWebElement addPayeeButton => driver.FindElement(By.XPath("//button[text()='Add']"));
        private IWebElement disabledAddPayeeButton => driver.FindElement(By.XPath("//button[contains(@class,'js-submit Button Button--primary Button--disabled')]"));
        private IWebElement payeeName => driver.FindElement(By.XPath("//input[@id='ComboboxInput-apm-name']"));
        private IWebElement nameHeader => driver.FindElement(By.XPath("//span[text()='Name']"));
        private IWebElement blankNameField => driver.FindElement(By.XPath("//input[@aria-label=\"Payee Name is a required field. Please complete to continue.\"]"));

        public void ClickAddButton()
        {
            if ((addButton).Enabled)
            {
                Wait.ForVisible(addButton);
                addButton.Click();
            }
            else
            {
                addButton.Click();
            }
        }

        public void ClickAddPayeeButton()
        {
            if ((addPayeeButton).Enabled)
                addPayeeButton.Click();
        }

        public void PayeeModal_ClickAddDisabled()
        {
            Wait.ForVisible(disabledAddPayeeButton);
            disabledAddPayeeButton.Click();
        }


        public void SelectPayeeName(string name)
        {
            driver.FindElement(By.XPath("//span[text()='" + name.ToUpper() + "']")).Click();
        }

        public void SetPayeeName(string name)
        {
            if ((payeeName).Enabled)
            {
                Wait.ForVisible(payeeName);
                payeeName.SendKeys(name);
            }
        }

        public string ValidatePayeeAdded(string actualResult)
        {
            string actualvalue = driver.FindElement(By.XPath("//span[text()='Payees']")).Text;
            Assert.IsTrue(actualvalue.Contains("Payees"), actualResult + " doesn't contains 'Payee..'");
            return actualvalue;
        }

        public string ValidatePayeesPage(string actualResult)
        {
            string actualvalue = driver.FindElement(By.XPath("//span[text()='Payees']")).Text;
            Assert.IsTrue(actualvalue.Contains("Payees"), actualResult + " doesn't contains 'Payees..'");
            return actualvalue;
        }

        public bool ValidateBlankPayeeName()
        {
            try
            {
                if (blankNameField.Displayed)
                    Console.WriteLine("Blank Name Field is Displayed");

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Blank Name Field is not displayed");
                return false;
            }
        }

        public IList GetPayeesNames()
        {
            IList<IWebElement> test = driver.FindElements(By.XPath("//span[@class='js-payee-name']"));
            IList payeeNames = new ArrayList();

            foreach (IWebElement list in test)
            {
                payeeNames.Add(list.Text);
                Console.WriteLine($"{list.Text}");
            }
            return payeeNames;
        }

        public void ClickNameHeader()
        {
            if ((nameHeader).Enabled)
            {
                Wait.ForVisible(nameHeader);
                nameHeader.Click();
            }
            else
            {
                nameHeader.Click();
            }
        }
    }
}