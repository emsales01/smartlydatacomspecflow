﻿using OpenQA.Selenium;
using Wait = SmartlyExercise.Utility.Wait;

namespace SmartlyExercise.Pages
{
    public class HomePage
    {
        public IWebDriver driver;

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement menuButton => driver.FindElement(By.XPath("//button[@aria-label='Attention! Main menu with 1 notification']//*[name()='svg']"));
        private IWebElement payeesButton => driver.FindElement(By.XPath("//span[normalize-space()='Payees']"));
        private IWebElement paymentsButton => driver.FindElement(By.XPath("//button[contains(@class,'Button Button--clean')]"));


        public void ClickOnMenu()
        {
            if ((menuButton).Enabled)
            {
                Wait.ForVisible(menuButton);
                menuButton.Click();
            }
            else
            {
                menuButton.Click();
            }
            Thread.Sleep(2000);
        }

        public void ClickPayees()
        {
            if ((payeesButton).Enabled)
            {
                Wait.ForVisible(payeesButton);
                payeesButton.Click();
            }
            else
            {
                payeesButton.Click();
            }
        }

        public void ClickPayments()
        {
            if ((paymentsButton).Enabled)
            {
                Wait.ForVisible(paymentsButton);
                paymentsButton.Click();
            }
            else
            {
                paymentsButton.Click();
            }
            Thread.Sleep(3000);
        }

        public double GetAccountBalance(String accountName)
        {
            String balance = driver.FindElement(By.XPath("//div[span[H3[@title='" + accountName + "']]]/span[@class='account-balance']")).Text;
            return Double.Parse(balance.Replace(",", ""));
        }
    }
}