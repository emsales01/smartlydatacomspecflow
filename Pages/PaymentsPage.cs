﻿using NUnit.Framework;
using OpenQA.Selenium;
using Wait = SmartlyExercise.Utility.Wait;

namespace SmartlyExercise.Pages
{
    public class PaymentsPage
    {
        public IWebDriver driver;

        public PaymentsPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement addButton(string fromOrTo) => driver.FindElement(By.XPath($"//button[@data-monitoring-label='Transfer Form {fromOrTo} Chooser']"));

        private IWebElement accountName => driver.FindElement(By.XPath("//input[@data-monitoring-label='Transfer Form Search']"));

        private IWebElement accountSelect(string account) => driver.FindElement(By.XPath($"//p[text()='{account}']"));

        private IWebElement amount => driver.FindElement(By.XPath("//input[@name='amount']"));

        private IWebElement transfer => driver.FindElement(By.XPath("//button//span[text()='Transfer']"));

        public void ClickFromAccount(string fromOrTo)
        {
            if ((addButton(fromOrTo)).Enabled)
            {
                Wait.ForVisible(addButton(fromOrTo));
                addButton(fromOrTo).Click();
            }
            else
            {
                addButton(fromOrTo).Click();
            }
        }

        public void SearchAccountName(string name)
        {
            Thread.Sleep(2000);
            if ((accountName).Enabled)
            {
                Wait.ForVisible(accountName);
                accountName.SendKeys(name);
            }
            else
            {
                accountName.SendKeys(name);
            }
        }

        public void SelectAccountName(string account)
        {
            if ((accountSelect(account)).Enabled)
            {
                accountSelect(account).Click();
            }
            else
            {
                Wait.ForVisible(accountSelect(account));
                accountSelect(account).Click();
            }
        }

        public void SelectAccount(String fromOrTo, String accountName)
        {
            ClickFromAccount(fromOrTo);
            SearchAccountName(accountName);
            SelectAccountName(accountName);
        }

        public void SetAmount(Double amountValue)

        {
            Thread.Sleep(2000);
            if ((amount).Enabled)
            {
                amount.SendKeys(amountValue.ToString());
            }
            else
            {
                Wait.ForVisible(accountName);
                amount.SendKeys(amountValue.ToString());
            }
        }

        public void ClickTransfer()
        {
            if ((transfer).Enabled)
            {
                transfer.Click();
            }
            else
            {
                Wait.ForVisible(transfer);
                transfer.Click();
            }
        }

        public void DoTransfer(String from, String to, Double amount)
        {
            SelectAccount("From", from);
            SelectAccount("To", to);
            SetAmount(amount);
            ClickTransfer();
        }

        public string ValidatePaymentAdded(string actualResult)
        {
            Thread.Sleep(3000);
            string actualvalue = driver.FindElement(By.XPath("//span[@role='alert']")).Text;
            Assert.IsTrue(actualvalue.Contains("Transfer successful"), actualvalue + " doesn't contains 'Other Text...'");
            return actualvalue;
        }
    }
}