using SmartlyExercise.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace SmartlyExercise.StepDefinitions
{
    [Binding]
    public class PaymentsStepDefinitions
    {
        public HomePage homePage;

        public PaymentsPage paymentsPage;

        //[ThreadStatic]
        private IWebDriver driver;

        public PaymentsStepDefinitions(IWebDriver driver)
        {
            this.driver = driver;
            paymentsPage = new PaymentsPage(driver);
            homePage = new HomePage(driver);
        }

        [When(@"Transfer (.*) from Everyday account to Bills account & Check the current balance of are correct")]
        public void WhenTransferFromEverydayAccountToBillsAccountCheckTheCurrentBalanceOfAreCorrect(int p0)
        {
            double transferAmount = 500.00;
            String fromAccountName = "Everyday";
            String toAccountName = "Bills ";
            int fromNewVal = 2000;
            int toNewVal = 920;

            double fromOldVal, toOldVal;

            fromOldVal = homePage.GetAccountBalance(fromAccountName);
            toOldVal = homePage.GetAccountBalance(toAccountName);

            paymentsPage.DoTransfer(fromAccountName, toAccountName, transferAmount);

            Assert.AreEqual(fromOldVal - transferAmount, fromNewVal);
            Assert.AreEqual(toOldVal + transferAmount, toNewVal);
        }

        [Then(@"Transfer successful message is displayed")]
        public void ThenTransferSuccessfulMessageIsDisplayed()
        {
            paymentsPage.ValidatePaymentAdded("Transfer Successful");
        }

        [When(@"Click Payments")]
        public void WhenClickPayments()
        {
            homePage.ClickPayments();
        }

        [When(@"Navigate to Payments Page")]
        public void WhenNavigateToPayments()
        {
            homePage.ClickOnMenu();
            homePage.ClickPayments();
        }
    }
}