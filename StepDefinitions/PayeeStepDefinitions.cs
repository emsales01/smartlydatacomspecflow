using System.Collections;
using OpenQA.Selenium;
using SmartlyExercise.Pages;
using TechTalk.SpecFlow;

namespace SmartlyExercise.StepDefinitions
{
    [Binding]
    public class PayeeStepDefinitions
    {
        public HomePage homePage;

        public PayeePage payeePage;

        //[ThreadStatic]
        private IWebDriver driver;

        public PayeeStepDefinitions(IWebDriver driver)
        {
            this.driver = driver;
            payeePage = new PayeePage(driver);
            homePage = new HomePage(driver);
        }

            [Given(@"Enter the Demo BNZ URL")]
            public void GivenEnterTheDemoBNZURL()
            {
                driver.Url = "https://www.demo.bnz.co.nz/client/";
            Thread.Sleep(4000);
        }

        [Then(@"(.*) added is displayed, and payee is added in the list of payees")]
        public void ThenPayeeAddedIsDisplayedAndPayeeIsAddedInTheListOfPayees(string p1)
        {
            payeePage.ValidatePayeeAdded(p1);
        }

        [Then(@"Validate errors are gone")]
        public void ThenValidateErrorsAreGone()
        {
            payeePage.ValidateBlankPayeeName();
        }

        [Then(@"Verify list is sorted in descending order")]
        public void ThenVerifyListIsSortedInDescendingOrder()
        {
            IList payeesNames = payeePage.GetPayeesNames();
            IList descending = new ArrayList(payeesNames);

            ArrayList.Equals(payeesNames, descending);
        }

        [Then(@"Verify Payees page is loaded")]
        public void ThenVerifyPayeesPageIsLoaded()
        {
            payeePage.ValidatePayeesPage("Payees");
        }

        [When(@"Click Add Button")]
        public void WhenClickAddButton()
        {
            payeePage.ClickAddButton();
        }

        [When(@"Click Add Disabled Payee Button")]
        public void WhenClickAddDisabledPayeeButton()
        {
            payeePage.PayeeModal_ClickAddDisabled();
        }

        [When(@"Click Add Payee Button")]
        public void WhenClickAddPayeeButton()
        {
            payeePage.ClickAddPayeeButton();
        }

        [When(@"Click Name header")]
        public void WhenClickNameHeader()
        {
            payeePage.ClickNameHeader();
        }

        [When(@"Click on Menu")]
        public void WhenClickOnMenu()
        {
            homePage.ClickOnMenu();
        }

        [When(@"Click on Payees")]
        public void WhenClickOnPayees()
        {
            homePage.ClickPayees();
        }

        [When(@"Enter the payee details (.*) and (.*)")]
        public void WhenEnterThePayeeDetailsPayeeAndPayee(string p1, string p2)
        {
            payeePage.SetPayeeName(p1);
            payeePage.SelectPayeeName(p2);
        }

        [When(@"Navigate to Payees Page")]
        public void WhenNavigateToPayees()
        {
            homePage.ClickOnMenu();
            homePage.ClickPayees();
        }

        [When(@"Validate Required Field Error")]
        public void WhenValidateRequiredFieldError()
        {
            payeePage.ValidateBlankPayeeName();
        }

        [When(@"Verify list is sorted in ascending order by default")]
        public void WhenVerifyListIsSortedInAscendingOrderByDefault()
        {
            IList payeesNames = payeePage.GetPayeesNames();
            IList ascending = new ArrayList(payeesNames);

            ArrayList.Equals(payeesNames, ascending);
        }
    }
}